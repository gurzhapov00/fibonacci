﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci.Helpers
{
    public static class FibonacciHelper
    {
        public static int[] GenerateFibonacciSequence(int[] seed)
        {
            List<int> sequence = new List<int> { seed[0], seed[1] };

            for (int i = 2; i < seed.Length; i++)
            {
                int nextValue = (sequence[i - 2] + sequence[i - 1]) % 1000;
                sequence.Add(nextValue);
            }

            return sequence.ToArray();
        }
    }
}
