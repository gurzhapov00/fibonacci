﻿namespace Fibonacci
{
    partial class FibonacciForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.seedTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.generateButton = new System.Windows.Forms.Button();
            this.fibonacciedArrayTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.saveChartButton = new System.Windows.Forms.Button();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите последовательность чисел";
            // 
            // seedTextBox
            // 
            this.seedTextBox.Location = new System.Drawing.Point(12, 29);
            this.seedTextBox.Multiline = true;
            this.seedTextBox.Name = "seedTextBox";
            this.seedTextBox.Size = new System.Drawing.Size(383, 47);
            this.seedTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(209, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "(вводите через пробелы)";
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(401, 29);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(110, 47);
            this.generateButton.TabIndex = 3;
            this.generateButton.Text = "Генерация";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // fibonacciedArrayTextbox
            // 
            this.fibonacciedArrayTextbox.Enabled = false;
            this.fibonacciedArrayTextbox.Location = new System.Drawing.Point(517, 29);
            this.fibonacciedArrayTextbox.Multiline = true;
            this.fibonacciedArrayTextbox.Name = "fibonacciedArrayTextbox";
            this.fibonacciedArrayTextbox.Size = new System.Drawing.Size(379, 47);
            this.fibonacciedArrayTextbox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(514, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(448, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Случайная последовательность при помощи запаздывающего генератора Фибоначчи";
            // 
            // saveChartButton
            // 
            this.saveChartButton.Location = new System.Drawing.Point(902, 29);
            this.saveChartButton.Name = "saveChartButton";
            this.saveChartButton.Size = new System.Drawing.Size(109, 47);
            this.saveChartButton.TabIndex = 6;
            this.saveChartButton.Text = "Сохранить график";
            this.saveChartButton.UseVisualStyleBackColor = true;
            this.saveChartButton.Click += new System.EventHandler(this.saveChartButton_Click);
            // 
            // chart
            // 
            chartArea2.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart.Legends.Add(legend2);
            this.chart.Location = new System.Drawing.Point(12, 82);
            this.chart.Name = "chart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart.Series.Add(series2);
            this.chart.Size = new System.Drawing.Size(999, 517);
            this.chart.TabIndex = 7;
            this.chart.Text = "chart";
            // 
            // FibonacciForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 611);
            this.Controls.Add(this.chart);
            this.Controls.Add(this.saveChartButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fibonacciedArrayTextbox);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.seedTextBox);
            this.Controls.Add(this.label1);
            this.Name = "FibonacciForm";
            this.Text = "FibonacciGenerator";
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox seedTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.TextBox fibonacciedArrayTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button saveChartButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
    }
}

